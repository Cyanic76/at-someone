# [at]someone
Yet another Discord bot bringing back the old April Fools [at]someone feature.

## What is [at]someone?
It was an old Discord april fools feature that got removed. It was meant to randomly ping someone. This bot aims to bring the feature back to Discord.

[Here's](https://r.cyanic.me/?id=D0003) Discord's [at]someone feature showcase.

## How to use?
Say `@someone` or mention the bot.

### Enable DND mode
This feature is coming soon. You'll be able to 'unsubscribe' from the bot's mentions if it gets too anoying for you.

In the meantime, you could try blocking the bot to avoid getting pinged.

## Links
[Invite the bot](https://r.cyanic.me/?id=D0001) |
[Support Server](https://discord.gg/zp8zF7Zx7y) |
[View on overwatch.wtf](https://r.cyanic.me/?id=D0002)

### Privacy & terms of use
No data is collected by this bot. Discord may collect data about messages, channels and more.

**You**, the User, are responsible for unwanted mentions or bot abuse.
